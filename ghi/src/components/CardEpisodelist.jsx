import Search from './Search'
import CardList from './Cardlist'

const CardEpisodelist = () => {
    return (
        <>
            <Search />
            <CardList />
        </>
    )
}

export default CardEpisodelist
